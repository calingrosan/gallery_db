import os, os.path
import uuid

class ImageHelper(object):
    def __init__(self, image_repository=None):
        self.image_repository = image_repository

    def _image_path_generator(self):
        ''' function for creating a guid for a file and move it to it's specific directory'''
        #generating file name and dir
        file_uuid = uuid.uuid4()
        dir = os.path.join(self.image_repository, str(file_uuid)[:1])
        proc_path = os.path.join(dir, str(file_uuid))

        if not os.path.exists(dir):
            os.makedirs(dir)

        return proc_path

    def image_content_generator(self, content):
        print content
        image_path = self._image_path_generator()

        with open(image_path, 'wb') as image_fh:
            image_fh.write(content.read())

        return image_path