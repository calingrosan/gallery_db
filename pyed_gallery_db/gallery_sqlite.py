from datetime import datetime
import sqlite3

class GalleryDb(object):
    def __init__(self, db_name):
        self.db = sqlite3.connect(db_name)
        self.c = self.db.cursor()
        self.c.row_factory = self.__dict_factory

        self.c.execute('''CREATE TABLE IF NOT EXISTS galleries
                     (id INTEGER PRIMARY KEY ASC, name TEXT)''')
        self.c.execute('''CREATE TABLE IF NOT EXISTS images
                             (name TEXT PRIMARY KEY, gallery_id INTEGER, 
                                FOREIGN KEY(gallery_id) REFERENCES galleries(id))''')

    def get_gallery(self, gallery_name):
        self.c.execute('SELECT a.name as image_name, a.gallery_id, b.name as gallery_name'
                       ' FROM images a JOIN galleries b on a.gallery_id = b.id WHERE b.name=?', (gallery_name,))

        return self.c.fetchall()

    def get_all_galleries(self):
        self.c.execute('SELECT name FROM galleries')

        return self.c.fetchall()
    
    def create_gallery(self, gallery_name='gallery_name', paths=None):
        if paths == None:
            paths = []
        print 'in create_galery' + gallery_name
        self.c.execute('SELECT id FROM galleries WHERE name=?', (gallery_name,))
        ex_id = self.c.fetchone()

        # insert gallery in db if it not exists already
        if not ex_id:
            self.c.execute('INSERT INTO galleries (name) VALUES (?)', (gallery_name,))
            ex_id = self.c.lastrowid

        # insert images into db
        paths_value = ','.join([ '("%s", %s)' % (path, ex_id) for path in paths ])
        self.c.execute('INSERT INTO images (name, gallery_id) VALUES %s' % paths_value)

        self.db.commit()

    def close(self):
        self.db.close()

    def __dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

def load_db(db_path):
    return GalleryDb(db_path)